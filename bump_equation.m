function [delta_r] = bump_equation(r)
    if nargin < 1, error('Not enough input arguments.'); end
    x = -r:r;
    delta_r = [exp(-1./(1-x.^2));exp(-1./(1-x.^2))];
end